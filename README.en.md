#Language Pack

####Installation
```php
    // Minimum requirement TP framework 6.1, PHP version 7.2.5
    composer require mark/tp-lang
```

####Instructions

    Publish language pack service registration through PHP think service: discover

    Execute installation configuration in markg\lang\Service and replace the binding class of framework language pack management class lang
    Language package loading path, unchanged for internal framework loading and system language package loading. Added the ability to create directory language packages by country under app/lang

    The loading function of other language packs remains unchanged, and extended language pack files can be specified. The country and language need to be concatenated [country language]

```PHP
    // YourProjectPath/config/lang.php
    $config = [
        'extend_list'=>[
            //'In en'=>__ DIR__. '/App/lang/in/en. php ',
            'In en'=>__ DIR__. '/App/lang/in_en. php ',
        ]
    ];
```

```php
    // You can switch between different countries through 
    markg\lang\facade\Lang::switchCountry('country','language');
    // Check if the key exists
    markg\lang\facade\Lang::has('key', $range='');
    // Get the value of the key
    markg\lang\facade\Lang::get('key', $vars=[], $range='')
```
