<?php
if (!function_exists('lang')) {
    /**
     * Get language variable values
     * @param string $name Language variable name
     * @param array $vars Dynamic variable value
     * @param string $lang Language
     * @return mixed
     */
    function lang(string $name, array $vars = [], string $lang = ''): mixed
    {
        return markg\lang\facade\Lang::get($name, $vars, $lang);
    }
}