<?php
declare(strict_types=1);

namespace markg\lang;

class Service extends \think\Service
{
    public function register(): void
    {
        $this->app->bind('lang', function() {
            $config = config('lang');
            return new Lang(app(), $config);
        });
    }

    public function boot(): void
    {
        $this->publish();
    }

    private function publish() {
        $version = config('lang.lang_version', '');
        if (empty($version) || $version !== Lang::LANG_VERSION) {
            $source = __DIR__ . '/config.php';
            $target = root_path('config') . 'lang.php';

            copy($source, $target);

            $source = __DIR__ . '/lang/en.php';
            $target = app_path('lang') . 'en.php';

            !is_dir(app_path('lang')) && mkdir(app_path('lang'), 0755, true);

            if (file_exists($target)) {
                $input = readline('A file will replace B file. Do you need to overwrite it? [yes/no, default no]');
                if ($input !== 'yes') {
                    return;
                }
            }

            copy($source, $target);
        }
    }
}